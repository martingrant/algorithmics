import java.util.*;
import java.io.*;
import networkFlow.*;

/**
 * The Class FordFulk.
 * Contains main part of the Ford-Fulkerson implementation and code for file input
 */
public class FordFulk {

	/** The name of the file that encodes the given network. */
	private String filename;
	
	/** The network on which the Ford-Fulkerson algorithm is to be run. */
	private Network net; 

	/**
	 * Instantiates a new FordFulk object.
	 * @param s the name of the input file
	 */
	public FordFulk(String s) {
		filename = s; // store name of input file
	}

	/**
	 * Read in network from file.
	 * See assessed exercise specification for the file format.
	 */
	public void readNetworkFromFile() {
		FileReader fr = null;
		// open file with name given by filename
		try {
			try {
				fr = new FileReader (filename);
				Scanner in = new Scanner (fr);

				// get number of vertices
				String line = in.nextLine();
				int numVertices = Integer.parseInt(line);

				// create new network with desired number of vertices
				net = new Network (numVertices);

				// now add the edges
				while (in.hasNextLine()) {
					line = in.nextLine();
					String [] tokens = line.split("[( )]+");
					// this line corresponds to add vertices adjacent to vertex u
					int u = Integer.parseInt(tokens[0]);
					// get corresponding Vertex object
					Vertex uu = net.getVertexByIndex(u);
					int i=1;
					while (i<tokens.length) {
						// get label of vertex v adjacent to u
						int v = Integer.parseInt(tokens[i++]);
						// get corresponding Vertex object
						Vertex vv = net.getVertexByIndex(v);
						// get capacity c of (uu,vv)
						int c = Integer.parseInt(tokens[i++]);
						// add edge (uu,vv) with capacity c to network 
						net.addEdge(uu, vv, c);
					}
				}
			}
			finally { 
				if (fr!=null) fr.close();
			}
		}
		catch (IOException e) {
			System.err.println("IO error:");
			System.err.println(e);
			System.exit(1);
		}
	}

	/**
	 * Executes Ford-Fulkerson algorithm on the constructed network net.
	 */
	public void fordFulkerson() {
		// complete this method as part of Task 2


		/** Calculates maximum flow f in network
		 G=(V,E)‏ with capacity function c */
		/*
		for ((u,v) : E)
		f(u,v)=0;
		for (;;)
		{  build residual graph G'=(V',E'), capacity function c';
			search for path P in G' from s to t; (†)
			if (such a path P found)‏    // augment f
			{  m = Math.min{c'(u,v) : (u,v)  P};
				for ((u,v) : P)‏
				if ((u,v)E && f(u,v) + m ≤ c(u,v))‏
				f(u,v) += m; // (u,v) forward edge
				else
				f(v,u) -= m; // (u,v) backward edge
			}
				else
				break;   // f is a maximum flow
			}*/



		// This is the main Ford-Fulkerson algorithm. The following loop will run until a maximum flow has been found.
		// Before entering here, the flow of all edges in the directed graph should be set to 0. This is taken care of
		// in the Edge constructor, where the flow is set to 0.
		for (;;)
		{
			// Create a residual graph, using the original network's as a base
			ResidualGraph resGraph = new ResidualGraph(net);

			// Find an augmenting path in the residual graph, stored as a linked list of edges
			// from the source vertex to the sink vertex
			LinkedList<Edge> path = resGraph.findAugmentingPath();

			// If a path has been found
			if (path != null)
			{
				// Find minimum capacity in path, by storing the capacity of all the path's edges in an ArrayList and
				// finding the minimum value of that list
				ArrayList<Integer> list = new ArrayList<Integer>();

				// Loop around all edges on the path and add the capacity of each to the ArrayList
				for (Edge edge : path)
					list.add(edge.getCap());

				// Variable m will be used as the minimum capacity, set it to the first value in the ArrayList
				int m = list.get(0);

				// Loop for each member of the ArrayList
				for (int i : list)
				{
					// If the current position in the ArrayList is less than the stored value, then overwrite the stored value
					// with the current position, being the new lowest capacity.
					if (list.get(i) < m)
						m = list.get(i);
				}

				for (Edge edge : path)
				{
					if (edge == null && edge.getFlow() + m <= edge.getCap())
					{
						edge.setFlow(edge.getFlow() + m);
					}
					else
					{
						edge.setFlow(edge.getFlow() - m);
					}
				}

				break;
			}
			else
			{
				// If an augmenting path cannot be found, then the network's maximum flow has been found and the algorithm
				// can now stop, breaking out of the loop.
				break;
			}
		}
	}

	/**
	 * Print the results of the execution of the Ford-Fulkerson algorithm.
	 */
	public void printResults() {
		if (net.isFlow()) {
			System.out.println("The assignment is a valid flow");
			System.out.println("A maximum flow has value: "+net.getValue());
			System.out.println("The flows along the edges are as follows:");
			net.printFlow();
		}
		else
			System.out.println("The assignment is not a valid flow");
	}
}