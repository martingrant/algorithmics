package networkFlow;
import java.util.*;

/**
 * The Class Network.
 * Represents a network - inherits from DirectedGraph class.
 */
public class Network extends DirectedGraph {

	/** The source vertex of the network. */
	protected Vertex source;
	
	/** The label of the source vertex. */
	protected int sourceLabel;
	
	/** The sink vertex of the network. */
	protected Vertex sink;
	
	/** The label of the sink vertex. */
	protected int sinkLabel;

	/**
	 * Instantiates a new network.
	 * @param n the number of vertices
	 */
	public Network (int n) {
		super(n);

		// add the source vertex - assumed to have label 0
		sourceLabel = 0;
		source = addVertex(sourceLabel);
		// add the sink vertex - assumed to have label numVertices - 1
		sinkLabel = numVertices - 1;
		sink = addVertex(sinkLabel);

		// add the remaining vertices
		for (int i = 1 ; i <=numVertices-2 ; i++) 
			addVertex(i);
	}

	/**
	 * Gets the source vertex.
	 * @return the source vertex
	 */
	public Vertex getSource() {
		return source;
	}

	/**
	 * Gets the sink vertex.
	 * @return the sink vertex
	 */
	public Vertex getSink() {
		return sink;
	}

	/**
	 * Adds the edge with specified source and target vertices and capacity.
	 * @param sourceEndpoint the source endpoint vertex
	 * @param targetEndpoint the target endpoint vertex
	 * @param capacity the capacity of the edge
	 */
	public void addEdge(Vertex sourceEndpoint, Vertex targetEndpoint, int capacity) { 
		Edge e = new Edge(sourceEndpoint, targetEndpoint, capacity);
		adjLists.get(sourceEndpoint.getLabel()).addLast(targetEndpoint);
		adjMatrix[sourceEndpoint.getLabel()][targetEndpoint.getLabel()]=e;
	}

	/**
	 * Returns true if and only if the assignment of integers to the flow fields of 
	 * each edge in the network is a valid flow.
	 * @return true, if the assignment is a valid flow
	 */
	public boolean isFlow() {
		// complete this method as part of Task 1

		// First check if the flow coming from the source and going to the sink match

		// Get the total flow coming from the source vertex
		int sourceFlow = getValue();

		// Store the edges leading to the sink vertex
		ArrayList<Edge> sinkEdges = new ArrayList<Edge>();

		// Loop around the adjacency matrix to check all edges
		for (int i = 0; i < adjMatrix.length; ++i)
		{
			for (int j = 0; j < adjMatrix[i].length; ++j)
			{
				// Check if the current edge is valid
				if (adjMatrix[i][j] != null)
				{
					// If the target vertex of the current edge is the sink vertex
					if (adjMatrix[i][j].getTargetVertex() == sink)
					{
						// Add the edge to the ArrayList
						sinkEdges.add(adjMatrix[i][j]);
					}
				}
			}
		}

		// Store the amount of flow going into the sink vertex
		int sinkFlow = 0;

		// Increment sinkFlow by the flow from each edge leading into the sink vertex
		for (Edge edge : sinkEdges)
		{
			sinkFlow += edge.getFlow();
		}

		// If the flow coming from the source, and the flow going into the sink do not match then there is not a valid flow
		if (Integer.compare(sourceFlow, sinkFlow) != 0)
		{
			return false;
		}

		// Secondly, check if any edge's flow is greater than its capacity

		// Loop around the adjacency matrix to check all edges
		for (int i = 0; i < adjMatrix.length; ++i)
		{
			for (int j = 0; j < adjMatrix[i].length; ++j)
			{
				// Check if the current edge is valid
				if (adjMatrix[i][j] != null)
				{
					// If the flow at this position is higher than it's capacity, then return false
					if (Integer.compare(adjMatrix[i][j].getFlow(), adjMatrix[i][j].getCap()) > 0)
					{
						return false;
					}
				}
			}
		}

		// Return true if the loops above find no instance of an edge's flow being greater than its capacity, i.e. there is a valid flow
		return true;
	}

	/**
	 * Gets the value of the flow.
	 * @return the value of the flow
	 */
	public int getValue() {
		// complete this method as part of Task 1

		// Set default flow value to 0
		int flow = 0;

		// Get the vertices adjacent to the source vertex
		LinkedList<Vertex> sourceAdjacents = getAdjList(source);

		// For each vertex adjacent to the source, add the value of its flow to the flow variable
		for (Vertex vertex : sourceAdjacents)
		{
			flow += getAdjMatrixEntry(source, vertex).getFlow();
		}

		// Return the flow value
		return flow;
	}

	/**
	 * Prints the flow.
	 * Display the flow through the network in the following format:
	 * (u,v) c(u,v)/f(u,v)
	 * where (u,v) is an edge, c(u,v) is the capacity of that edge and f(u,v) 
         * is the flow through that edge - one line for each edge in the network
	 */
	public void printFlow() {
		// complete this method as part of Task 1

		// Loop around the adjacency matrix
		for (int i = 0; i < adjMatrix.length; ++i)
		{
			for (int j = 0; j < adjMatrix[i].length; ++j)
			{
				// Check if the current edge is valid
				if (adjMatrix[i][j] != null)
				{
					// Get the vertices at either side of the current edge
					int u = adjMatrix[i][j].getSourceVertex().getLabel();
					int v = adjMatrix[i][j].getTargetVertex().getLabel();

					// Get the capacity and flow of the current edge
					int cap = adjMatrix[i][j].getCap();
					int flow = adjMatrix[i][j].getFlow();

					// Print out the vertices, and the capacity and flow of this edge in the form - (u,v) c(u,v)/f(u,v)
					System.out.println("(" + u + "," + v + ") " + cap + "/" + flow);
				}
			}
		}
	}
}
