package networkFlow;

import java.util.*;

/**
 * The Class ResidualGraph.
 * Represents the residual graph corresponding to a given network.
 */
public class ResidualGraph extends Network {
    /**
     * Instantiates a new ResidualGraph object.
     * Builds the residual graph corresponding to the given network net.
     * Residual graph has the same number of vertices as net.
     *
     * @param net the network
     */
    public ResidualGraph(Network net) {
        super(net.numVertices);
        // complete this constructor as part of Task 2

        // Copy the data from the net parameter to this graph, so that it is an exact copy
        source = net.getSource();
        sourceLabel = net.sourceLabel;
        sink = net.getSink();
        sinkLabel = net.sinkLabel;

        vertices = net.vertices;
        adjLists = net.adjLists;
        adjMatrix = net.adjMatrix;
    }

    /**
     * Find an augmenting path if one exists.
     * Determines whether there is a directed path from the source to the sink in the residual
     * graph -- if so, return a linked list containing the edges in the augmenting path in the
     * form (s,v_1), (v_1,v_2), ..., (v_{k-1},v_k), (v_k,t); if not, return an empty linked list.
     *
     * @return the linked list
     */
    public LinkedList<Edge> findAugmentingPath() {
        // complete this method as part of Task 2

        // Use a linked list to store edges as the search traverses through the graph
        LinkedList<Edge> path = new LinkedList<Edge>();

        // Use a stack for the depth-first search
        Stack<Vertex> stack = new Stack<Vertex>();

        // Push the source vertex onto the stack as the first elemetn
        stack.push(source);

        // Create a temporary vertex to traverse through the graph
        Vertex currentVertex;

        // Use an int array to store which vertices have been visited, 0 = not visited, 1 = visited
        // The vertex indices are matched with the vertices themselves, i.e. index 3 is vertex 3 in the graph
        int visitedVertices[] = new int[numVertices];

        // Loop whilst the stack is not empty
        while (stack.empty() == false)
        {
            // Pop and store the top of the stack
            currentVertex = stack.pop();

            // If the current vertex is the sink, we are done
            if (currentVertex == sink)
                break;

            // Check if the current vertex has been visited or not
            if (Integer.compare(visitedVertices[currentVertex.getLabel()], 0) == 0)
            {
                // Entering this if statement means it has not been visited before, now mark it as visited
                visitedVertices[currentVertex.getLabel()] = 1;

                // Loop for all adjacent vertices
                for (Vertex vertex : getAdjList(currentVertex))
                {
                    // Get the edge linking the current vertex and this adjacent vertex
                    Edge edge = getAdjMatrixEntry(currentVertex, vertex);

                    // If there is available space on this edge (cap - flow > 0), push it to the stack and add it to the path list
                    if (Integer.compare(edge.getCap() - edge.getFlow(), 0) > 0 )
                    {
                        stack.push(vertex);
                        path.add(getAdjMatrixEntry(currentVertex, vertex));
                    }

                    // Break if we are at the sink vertex
                    if (vertex == sink)
                        break;
                }
            }
        }
        return path;
    }
}






